最近做系统重构，计划将多个系统的公共部分抽取出来作为一项公共服务，为以后项目维护和横向扩展奠定基础。

常用的服务发布方式有RMI / HTTPInvoker / Hessian / Burlap，关于这几类java远程服务的性能比较和优缺点大家可参考：http://www.cnblogs.com/jifeng/archive/2011/07/20/2111183.html ，本文着重讲解怎样使用自定的注解标签，结合SPRING将服务方便的发布出去。

系统的使用介绍请查看我的osc博客：http://my.oschina.net/damihui/blog/354055