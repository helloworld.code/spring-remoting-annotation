package me.bbvip.springremoting.burlap;

import java.util.Date;

/**
 * Created by Administrator on 2014/12/9.
 */
public interface BurlapDateService {
    public Date getDate();
}
