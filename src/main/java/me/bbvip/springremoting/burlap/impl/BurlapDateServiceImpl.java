package me.bbvip.springremoting.burlap.impl;

import me.bbvip.springremoting.burlap.BurlapDateService;
import org.springframework.remoting.RemoteService;
import org.springframework.remoting.ServiceType;

import java.util.Date;

/**
 * Created by Administrator on 2014/12/9.
 */
@RemoteService(serviceInterface = BurlapDateService.class, serviceType = ServiceType.BURLAP)
public class BurlapDateServiceImpl implements BurlapDateService {
    @Override
    public Date getDate() {
        return new Date();
    }
}
