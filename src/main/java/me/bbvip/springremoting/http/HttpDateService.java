package me.bbvip.springremoting.http;

import java.util.Date;

public interface HttpDateService {
    public Date getDate();
}
