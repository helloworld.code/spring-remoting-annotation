package me.bbvip.springremoting.http.impl;

import java.util.Date;

import me.bbvip.springremoting.http.HttpDateService;
import org.springframework.remoting.RemoteService;
import org.springframework.remoting.ServiceType;

@RemoteService(serviceInterface = HttpDateService.class, serviceType = ServiceType.HTTP)
public class HttpDateServiceImpl implements HttpDateService {

    @Override
    public Date getDate() {
        return new Date();
    }
}
