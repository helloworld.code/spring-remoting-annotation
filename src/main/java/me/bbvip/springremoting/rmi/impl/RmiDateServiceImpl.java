package me.bbvip.springremoting.rmi.impl;

import me.bbvip.springremoting.rmi.RmiDateService;
import org.springframework.remoting.RemoteService;
import org.springframework.remoting.RmiServiceProperty;
import org.springframework.remoting.ServiceType;

import java.util.Date;

/**
 * Created by Administrator on 2014/12/8.
 */
@RemoteService(serviceInterface = RmiDateService.class, serviceType = ServiceType.RMI)
@RmiServiceProperty(registryPort = 1099)
public class RmiDateServiceImpl implements RmiDateService{
    @Override
    public Date getDate() {
        return new Date();
    }
}
