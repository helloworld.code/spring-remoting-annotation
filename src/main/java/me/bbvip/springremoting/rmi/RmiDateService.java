package me.bbvip.springremoting.rmi;

import java.util.Date;

/**
 * Created by Administrator on 2014/12/8.
 */
public interface RmiDateService {
    public Date getDate();
}
