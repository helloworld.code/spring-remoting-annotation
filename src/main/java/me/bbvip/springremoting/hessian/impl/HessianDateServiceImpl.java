package me.bbvip.springremoting.hessian.impl;

import me.bbvip.springremoting.hessian.HessianDateService;
import org.springframework.remoting.RemoteService;
import org.springframework.remoting.ServiceType;

import java.util.Date;

/**
 * Created by Administrator on 2014/12/9.
 */
@RemoteService(serviceInterface = HessianDateService.class, serviceType = ServiceType.HESSIAN)
public class HessianDateServiceImpl implements HessianDateService {
    @Override
    public Date getDate() {
        return new Date();
    }
}
