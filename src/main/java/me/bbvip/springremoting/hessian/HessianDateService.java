package me.bbvip.springremoting.hessian;

import java.util.Date;

/**
 * Created by Administrator on 2014/12/9.
 */
public interface HessianDateService {
    public Date getDate();
}
