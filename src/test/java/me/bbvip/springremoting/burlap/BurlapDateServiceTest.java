package me.bbvip.springremoting.burlap;

import me.bbvip.springremoting.ServerRunner;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;

public class BurlapDateServiceTest extends ServerRunner {

    @Autowired
    private BurlapDateService burlapDateService;
    @Test
    public void testGetDate() throws Exception {
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(burlapDateService.getDate()));
    }
}