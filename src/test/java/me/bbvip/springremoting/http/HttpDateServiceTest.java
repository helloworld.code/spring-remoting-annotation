package me.bbvip.springremoting.http;

import me.bbvip.springremoting.ServerRunner;
import org.junit.Test;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;

public class HttpDateServiceTest extends ServerRunner {

    @Resource(name = "httpDateService")
    private HttpDateService httpDateService;

    @Test
    public void getGetDate() {
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(httpDateService.getDate()));
    }
}