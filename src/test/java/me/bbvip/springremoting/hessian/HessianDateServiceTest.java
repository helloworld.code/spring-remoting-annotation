package me.bbvip.springremoting.hessian;

import me.bbvip.springremoting.ServerRunner;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;

public class HessianDateServiceTest extends ServerRunner {

    @Autowired
    private HessianDateService hessianDateService;
    @Test
    public void testGetDate() throws Exception {
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(hessianDateService.getDate()));
    }
}