package me.bbvip.springremoting.rmi;

import me.bbvip.springremoting.ServerRunner;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;

public class RmiDateServiceTest extends ServerRunner {

    @Autowired
    private RmiDateService rmiDateService;
    @Test
    public void testGetDate() throws Exception {
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rmiDateService.getDate()));
    }
}